---
Week: 48
tags:
- TCP
- Sockets
- HTTP(S)
- Protocols
- Socket
- Web scraping
- RFC
---

# Exercises for ww48

## Exercise 1 - PY4E chapter 12 knowledge sharing (Group) 

### Information

In your team discuss your understanding of:

2. What are RFC's (Request for comment) ?
3. What are Protocols ? (especially HTTP and HTTPS)
4. What are sockets ?
5. What is web scraping
6. When do you use a binary file instead of a text file ?
7. How does the things you have already learned, fit into chapter 12 ?

### Exercise instructions

Agree in your team on answers and complete the quiz at: [https://forms.gle/ZqjvsdV997jQALyd9](https://forms.gle/ZqjvsdV997jQALyd9)

You have 30 minutes to complete exercise 0

*Remember to note your answers in your team's shared document*

## HTTP Video break

Together we will watch:

What happens when you click a link [https://youtu.be/keo0dglCj7I](https://youtu.be/keo0dglCj7I)

## Exercise 2 - Pair programming challenge

In pairs of 2 students, write a number guessing game for 2 players, human vs. computer.  
Use functions where possible to avoid DRY (don't repeat yourself)   
Use flow charts and pseudo code to get an overview before implementing the code.   

### Exercise instructions

Requirements for the number guessing game are:

1. On game start calculate a random number between 0 and 9
2. The computer will give you 3 attempts to guess the number, otherwise the computer wins the round
3. If the user enters anything else than a number print the message `Thats not even a number.... restarting` and then restart the program
4. If the user wins print `Damn, you won!, try again? (y/n)`   
5. If the computer wins, print `na na I won :-P - try again? (y/n)` 
6. If user enters `y` at the end of a round, restart the program
7. If the user enters `n` at the end of a round, print the total rounds score between human and computer

Test the program with following test cases:

**test 1 - Invalid user input**  
input:
```
enter your guess between 0 and 9 > ulahhhh
```

output: 
```
Thats not even a number.... restarting
enter your guess between 0 and 9 >
``` 

**test 2 - 1st or 2nd wrong guess**  
input:
```
enter your guess between 0 and 9 > 3
```

output: 
```
wrong guess, try again!
enter your guess between 0 and 9 >
``` 

**test 3 - 3rd wrong guess**  
input:
```
enter your guess between 0 and 9 > 3
```

output: 
```
na na I won :-P - try again? (y/n) >
```

**test 4 - correct guess**  
input:
```
enter your guess between 0 and 9 > 4
```

output: 
```
Damn, you won!, try again? (y/n) >
```

**test 5 - end game**  
input:
```
Damn, you won!, try again? (y/n) > n
```

output: 
```
computer wins: 1 human wins: 1 see ya...
```
**bonus if you finish early:**  

1. Implement functionality that will stop the program when either human or computer reach 5 won rounds
2. If the user enters an arbitrary string which has a number, extract the number from the string and use it as a guess. ie. `sdoijwer6poi`  

You are free to search relevant information on the internet to solve the bonus tasks.  
If you solve both of the bonus tasks and show it in class, you will be rewarded with a cup of coffee

## Exercise 3 - Python for everybody chapter exercises

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete chapter 12 exercises, in Python For Everybody.
**remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**

\pagebreak