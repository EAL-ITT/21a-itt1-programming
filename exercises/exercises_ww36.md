---
Week: 36
tags:
- Python
- Gitlab
- Git
---

# Exercises for ww36


## Exercise 1 - Install Python + Hello World

### Information

This exercise guides you on how to install python on your computer and how to write your first python program.

### Exercise instructions

1. Download the latest version of [Python](https://www.python.org/downloads/)
2. Open the installation file and follow the installer instructions  
![python installer](install_py.PNG "Python installer")  

3. Check that Python is installed by opening a `CMD` [prompt](https://www.lifewire.com/how-to-open-command-prompt-2618089) and write `py --version`    
If sucess you should see the python version in the `CMD` prompt  
![python install success](cmd_success.PNG "python install success")

4. Watch the video [Hello World in Python](https://youtu.be/KOdfpbnWLVo) to get background information on the rest of the exercise.

5. In `CMD` prompt write `python` to enter the [REPL](https://pythonprogramminglanguage.com/repl/) which is Pythons interactive shell. REPL is short for Read, Eval, Print and Loop.  
![REPL](REPL.PNG "REPL")

6. Write  
```python 
print ("Hello World")
``` 
and press enter  
![Hello World](hello_world.PNG "Hello World")  
If you see Hello World printed in REPL congratulations! You have written your first Python program.

Your program is not saved to the harddrive and can only be executed one time.
To write your program to the harddrive you need to make a python script. python scripts has the extension .py

7. Exit REPL by writing `quit()` and press enter

8. Create a folder on your computer called "`full_name`_programming_exercises"  
**hint** replace `full_name` with your first and last name in lower case,  
ie. `nikolaj_simonsen_programming_exercises`  
Remember the underscores to avoid white space in the name.

10. Click the windows start button and enter `idle` to find the Integrated Development Environment (IDE) that is installed with Python. Open it by clicking the icon.  

10. Create a new file from the file menu  
![IDLE](IDLE.PNG "IDLE")

11. Save the file as `000_hello_world.py` in your "`full_name`_programming_exercises" folder  

12. In `000_hello_world.py` write 
```python
print ("Hello World")
``` 
13. Save `000_hello_world.py` using the file menu 

13. Press `F5` to run the python script  
![hello world in idle](idle_hello_world.PNG "hello world in idle")

14. Open a `CMD` prompt from within your "`full_name`_programming_exercises" folder by navigating to the folder and write `CMD` in the address bar followed by enter  
![cmd prompt from folder](cmd_folder.PNG "cmd prompt from folder")

15. Execute `000_hello_world.py` with the command `python 000_hello_world.py`  
    This tells windows to run your script in Python  
    ![hello world in CMD](cmd_hello_world.PNG "hello world in CMD")

    You should see Hello World as the output

## Exercise 2: Gitlab setup

### Information

This exercise guides you on a gitlab project for hand-ins during the rest of the programming course.

### Exercise instructions

**Part 1**

You can skip this part if you did it previously or already have a gitlab account

1. Follow the guide at [https://npes.gitlab.io/claat-generator/gitlab_daily_workflow/index.html#0](https://npes.gitlab.io/claat-generator/gitlab_daily_workflow/index.html#0) to setup GIT and a Gitlab account

**Part 2**

2. Request access to the gitlab group [https://gitlab.com/21a-itt1-programming-exercises](https://gitlab.com/21a-itt1-programming-exercises)  
Help on requesting access his here: [https://docs.gitlab.com/ee/user/group/#request-access-to-a-group](https://docs.gitlab.com/ee/user/group/#request-access-to-a-group) 

2. Wait for approval from the group owner (NISI)

3. Setup your gitlab project in the gitlab group [https://gitlab.com/21a-itt1-programming-exercises](https://gitlab.com/21a-itt1-embedded-exercises)  

    **Naming convention (must be followed strictly!):** 
       
    `<firstname>-<lastname>-programming-exercises`  
    
    **Replace `<firstname>` `<lastname>` with your firstname and lastname, remember the hyphens between words**

## Exercise 3: Python For Everybody, chapter 1 exercises

### Exercise instructions

Complete Python For Everybody, chapter 1 exercises and document it in a .py file on gitlab  

Exercises are in chapter 1 of the PY4E book