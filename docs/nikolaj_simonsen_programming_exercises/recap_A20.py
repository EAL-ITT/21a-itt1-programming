# This is a List [] and it is mutable
#my_list = ['string', [1,2,3,4], 1,2,3,]

# This is a dictionary {'key': 'value'} and it is mutable 
#my_dictionary = {'car1': 'VW', 'car2': 'Tesla', 'carlist': [1,2,3,4], 'number1': 1}

# print(my_dictionary['car1'])

# for key, val in my_dictionary.items():
#     print(val)

# This is a Tuple () and it is immutable
#my_tuple = ('johnny', 'peter', 1, 2, ['1', 4, '5'])

# for loop
# for item in my_list:
#     print(item)

# x = 10
# while x >= 0:
#     print(x)
#     x = x - 1

# function
# def add_two_numbers (number1, number2):
#     result = number1 +  number2
#     return result

# calculation = add_two_numbers(2, 2)
# print(f'The result of addition is: {calculation}') 

# Class
# class Train():

#     def __init__(self, name, count = 0):
#         self.name = name
#         self.count = count

#     def hello(self):
#         print(f'Hello, I am a train, my name is {self.name}')

#     def counter(self):
#         return self.count

# train1 = Train('TukTuk')
# train2 = Train('Thomas')

# print(train2.counter())
# train2.count = 5
# print(train2.counter())

# List, it is mutable
# my_list = ['string', [1, 2, 3], 2.15, ('one', 'two'), True, False]
# # print(my_list)
# for item in my_list:
#     print(item)

# Dictionaries, they are mutable
#my_dict = {'integer': 22, 45: 45, 'other_datatype': 'updog'}
#print(my_dict['other_datatype'])
# for key, value in my_dict.items():
#     print(value, key)

# Tuples, they are immutable
my_tuple = ('Tina', 'Turner', [1, 2, 3], {'key': 'value'})
# print(my_tuple[-1])
# for item in my_tuple:
#     print(item)
# (item0, item1, item2, item3) = my_tuple
# print(item2)

# Set
# my_set = {'item1', 'item2', 'item3', 'item3'}
# for item in my_set:
#     print(item)

# Classes

# class Calculator():

#     def __init__(self, hello_msg):
#         self.hello_msg = hello_msg
    
#     def add_two_numbers(self, number_1, number_2):
#         result = number_1 + number_2
#         return result

# my_calculator = Calculator('Welcome to the first calculator')

# my_2nd_calculator = Calculator('Welcome to the second calculator')

# calculation = my_calculator.add_two_numbers(2, 2) 
# calculation2 = my_2nd_calculator.add_two_numbers(4 , 4)

# print(f'{my_calculator.hello_msg} 2 + 2 is = {calculation}')
# print(f'{my_2nd_calculator.hello_msg} 4 + 4 is = {calculation2}')

#while loop 
# counter = 10
# while counter > 0:
#     print(counter)
#     counter -= 1