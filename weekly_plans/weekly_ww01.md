---
Week: 01
Content:  Databases
Material: See links in weekly plan
Initials: AMNI1
---

# Week 01 ITT1-programming - Using databases and SQL

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Sqlite Python exercises 

### Learning goals

The student can:

* Create SQL databases
* Use sqlbrowser
* Use the sqlite module in Python3
* Create, read, update and delete (CRUD) tables using SQL
* Query data using SQL

The student knows: 

* The most important SQL commands
* The basics of data modelling
* One-to-many and many-to-many relationships

## Deliverables

* Exercises completed and documented on gitlab

## Schedule

* 08:15 Introduction to the day 
* 08:30 Exercises with lecturer (K1)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K1)
* 13:45 Exercises without lecturer (K2/K3)
* 15:30 End of the day

## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww01](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww01)

## Comments

[Python3 sqlite module documentation](https://docs.python.org/3/library/sqlite3.html)  

PY4E Chapter 15 video lessons:

* part 1 - introduction [https://youtu.be/3RMPveOMd0k](https://youtu.be/3RMPveOMd0k)
* part 2 - Single Table SQL [https://youtu.be/yRJE-nk20sM](https://youtu.be/yRJE-nk20sM)
* part 3 - Complex models [https://youtu.be/rHjRpYUl5eg](https://youtu.be/rHjRpYUl5eg)
* part 4 - Database relationships [https://youtu.be/PgE--P-ZWvU](https://youtu.be/PgE--P-ZWvU)
* part 5 - Foreign keys [https://youtu.be/GfuH_8uH16k](https://youtu.be/GfuH_8uH16k)
* part 6 - Join [https://youtu.be/zMOSVrb82iU](https://youtu.be/zMOSVrb82iU)
* part 7 - many to many [https://youtu.be/uq_Wf4nuXqE](https://youtu.be/uq_Wf4nuXqE)

Socratica:

* SQL [https://www.youtube.com/playlist?list=PLlsaguFT2REiDlkzxso6xS2-wTHG0oqzs](https://www.youtube.com/playlist?list=PLlsaguFT2REiDlkzxso6xS2-wTHG0oqzs)

More quizzes at realpython [https://realpython.com/quizzes/](https://realpython.com/quizzes/)

