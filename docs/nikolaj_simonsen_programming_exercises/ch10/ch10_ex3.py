"""
Exercise 3: Write a program that reads a file and prints the letters
in decreasing order of frequency.
Your program should convert all the input to lower case and only count the letters a-z.
Your program should not count spaces, digits, punctuation, or anything other than the letters a-z.
Find text samples from several different languages and see how letter frequency varies between languages.
Compare your results with the tables at https://wikipedia.org/wiki/Letter_frequencies.

Pseudo code:
0. open file
1. read file line
2. skip empty lines
3. remove newline \n
4. remove spaces
5. remove digits
6. remove punctuation
7. convert to lower case
8. make histogram of a-z
9. order by letter frequency
10. test with english.txt and danish.txt
"""

import string
from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths
files_path = Path(str(Path.cwd()) + '/docs/nikolaj_simonsen_programming_exercises/files/language_txt/')

# open file
file_name = input('Enter a file name: ')
if len(file_name) <=1:
    file_name = "english.txt"
try:
    lang_file = open(files_path /  file_name, encoding="utf-8") #utf-8 required for danish chars

    letter_histogram = dict() # to hold letter count and letter

    # read file line
    for line in lang_file:
        # skip empty line
        if not line.strip():
            continue
        else:
            line = line.rstrip("\n") # remove newline \n
            line = line.replace(" ", "") # remove spaces
            line = line.replace("–", "")  # remove weird char in danish.txt
            line = line.translate(line.maketrans("", "", string.digits)) # remove digits
            line = line.translate(line.maketrans("", "", string.punctuation)) # remove punctuation
            line = line.lower() # convert to lower case

            # make histogram of a-z, store in dictionary
            for letter in line:
                if letter not in letter_histogram:
                    letter_histogram[letter] = 1
                else:
                    letter_histogram[letter] += 1

    lang_file.close() # close the file, not needed anymore
    lst = list()

    # add dictionary items as tuples to a list
    for letter, count in list(letter_histogram.items()):
        lst.append((count, letter))

    # sort the list
    lst.sort(reverse=True)

    # write out items in list
    for item in lst:
        print(*item) # * is unpacking the tuple

except FileNotFoundError:
    print("File not found")

