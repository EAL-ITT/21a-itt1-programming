---
Week: 39
Content:  Functions
Material: See links in weekly plan
Initials: NISI
---

# Week 39 ITT1-programming - Functions

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Read chapter 4 in Python For Everybody
* Chapter 4 exercises, in Python For Everybody, completed

### Learning goals

The student can implement:

* Functions
* Built in functions
* Math functions
* User defined functions
* Function parameters and arguments

## Deliverables

* Chapter 4 exercises, in Python For Everybody, documented on Gitlab

## Schedule

* 8:15 Introduction to the day (K1)
* 8:30 Code review (K1) 
    Code review of chapter 3 exercises from py4e
* 9:00 Preparation for hands-on time (K2)  
    Either/or:
    * Read chapter 4
    * Watch chapter 4 videos 
* 10:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises without lecturer (K2/K3)
* 15:30 End of day


## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww39](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww39)

## Comments

PY4E Chapter 4 video lessons:

* part 1 [https://youtu.be/5Kzw-0-DQAk](https://youtu.be/5Kzw-0-DQAk)
* part 2 [https://youtu.be/AJVNYRqn8kM](https://youtu.be/AJVNYRqn8kM)