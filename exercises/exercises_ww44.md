---
Week: 44
tags:
- Dictionaries
- Programming challenge
---

# Exercises for ww44
 
The cooperative learning structures can be found at [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf)

## Exercise 1 - PY4E chapter 9 knowledge QUIZ

This is a teacher paced quiz with questions about PY4E chapter 9 - Dictionaries

Go to [https://b.socrative.com/login/student/](https://b.socrative.com/login/student/) and write **simonsen7368** as the room number

Answer the first question and wait. When everyone have answered a question you will get the next question.

## Exercise 2 - Work on your own challenge

As promised, you have time scheduled in class to work on you own challenge.
Use the opportunity to ask your classmates about advice if you have problems or would like advice to solve a particular problem in your challenge.

## Exercise 3 - Python for everybody chapter exercises

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete chapter 9 exercises, in Python For Everybody.
**remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**

\pagebreak