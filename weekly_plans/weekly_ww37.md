---
Week: 37
Content:  Programming basics
Material: See links in weekly plan
Initials: NISI
---

# Week 37 ITT1-programming - Basics

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Read chapter 2 in Python For Everybody
* Python For Everybody - Chapter 2 exercises completed 

### Learning goals

* GIT (LGS1, LGS2, LGS3, LGC2)
    * Level 1: The student knows what GIT version control is
    * Level 2: The student knows the basic GIT commands
    * Level 3: The student can perform daily GIT operations


* PY4E Chapter 2 (LGK7, LGK8, LGS2, LGS3, LGC1, LGC2)  
    The student has basic knowledge about:
    * commenting code
    * input and output
    * value types
    * operators
    * variables
    * statements
    * reserved words
    * mnemonic naming
    * concatenation

## Deliverables

* Chapter 2 exercises, in Python For Everybody, documented on Gitlab

## Schedule

* 8:15 Introduction to the day (K1)
* 8:30 Code review (K1) 
    Code review of chapter 1 exercises from py4e
* 9:00 Preparation for hands-on time (K2)  
    Either/or:
    * Read chapter 2
    * Watch chapter 2 videos 
* 10:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 13:00 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww37](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww37)

## Comments

PY4E Chapter 2 video lessons:

* part 1 [https://youtu.be/7KHdV6FSpo8](https://youtu.be/7KHdV6FSpo8)
* part 2 [https://youtu.be/kefrGMAglGs](https://youtu.be/kefrGMAglGs)

Socratica videos covering some topics from chapter 2 and 3:

* Python strings [https://youtu.be/iAzShkKzpJo](https://youtu.be/iAzShkKzpJo)
* Numbers in Python Version 3 [https://youtu.be/_87ASgggEg0](https://youtu.be/_87ASgggEg0)
* Arithmetic in Python 3 [https://youtu.be/Aj8FQRIHJSc](https://youtu.be/Aj8FQRIHJSc)