---
Week: 43
tags:
- List
- Programming challenge
---

# Exercises for ww43

## Exercise 1 - Python quiz

It is important to notice your own progress when learning. This exercise will help you do that. 

### Exercise instructions

Complete the Python quiz [https://www.w3schools.com/python/python_quiz.asp](https://www.w3schools.com/python/python_quiz.asp) 

You are allowed to google for answers, but remember to finish in time.  
When the quiz is done, review your answers and note the ones you couldn't answer, we will revisit the quiz later in the semester.

You have 20 minutes. 

## Exercise 2 - PY4E chapter 8 knowledge (group)

### Information

In your team discuss your understanding of: 

* How do you create a list ?
* What element types can a list hold ?
* What does it mean, that lists are mutable ?
* How do you traverse a list ? 
* How can you concatenate lists ?
* What is the output of ```[7,6,5] * 2``` 
* How do you slice a list ?
* What is the return value of the pop list method ?
* How do you sum a list of numbers ?
* If you have multiple variable assigned to the same list, what impact will it have to change one of the variables ?


### Exercise instructions

Agree in your team on answers and complete the quiz at: [https://forms.gle/dYELUGNoTfdtLedH7](https://forms.gle/dYELUGNoTfdtLedH7)

You have 30 minutes to complete exercise 2  

*Remember to note your answers in your team's shared document* 

## Exercise 3 - Find your own challenge

One of the best ways to get motivated learning new things is to find something you want to accomplish, in this case by using Python.  
I want you to find somtehing you want to accomplish using Python, something you would like to be able to code!  

Don't be afraid to pick something that you dont't know how to do with Python yet, you will learn while you do it and I will help you.

Examples:  
* Read your google calendar from a python script
* Make your own electronics calculator
* Count how many lines of Python code you have written
* Track which websites you spend time visiting
* Write a game
* A discord bot
* An element bot [https://element.io/](https://element.io/)

* You can also search online for challenges:  
* realpython [https://realpython.com/what-can-i-do-with-python/](https://realpython.com/what-can-i-do-with-python/)  
* codementor [https://www.codementor.io/ilyaas97/6-python-projects-for-beginners-yn3va03fs](https://www.codementor.io/ilyaas97/6-python-projects-for-beginners-yn3va03fs)  
* codeclubprojects [https://codeclubprojects.org/en-GB/python/](https://codeclubprojects.org/en-GB/python/)  
* Raspberry Pi projects book 4 [https://magpi.raspberrypi.com/books/projects-4](https://magpi.raspberrypi.com/books/projects-4)  
* Raspberry Pi projects book 5 [https://magpi.raspberrypi.com/books/projects-5](https://magpi.raspberrypi.com/books/projects-5)
* AIY Voice projects [https://magpi.raspberrypi.com/books/essentials-aiy-v1](https://magpi.raspberrypi.com/books/essentials-aiy-v1)  
* Code music with sonic Pi [https://magpi.raspberrypi.com/books/essentials-sonic-pi-v1](https://magpi.raspberrypi.com/books/essentials-sonic-pi-v1)
* Make games with Python [https://magpi.raspberrypi.com/books/essentials-games-vol1](https://magpi.raspberrypi.com/books/essentials-games-vol1)
* More RPi books [https://magpi.raspberrypi.com/books](https://magpi.raspberrypi.com/books)
* Python Discord Bot [https://realpython.com/how-to-make-a-discord-bot-python/](https://realpython.com/how-to-make-a-discord-bot-python/)

You can also search the internet for "python projects"

You have 30 minutes to find a challenge.  
After the exercise we will do a round on class where you present your chosen challenge.
Please document your own challenge as a seperate project in the gitlab group [https://gitlab.com/20a-itt1-programming-exercises](https://gitlab.com/20a-itt1-programming-exercises) remember to give the project a mnemonic name!  
In the project write a brief overview of the challenge you have chosen.  

In week 47 there will be an OLA about your own challenge [https://eal-itt.gitlab.io/20a-itt1-programming/other-docs/20A_ITT1_PROGRAMMING_OLA16.html](https://eal-itt.gitlab.io/20a-itt1-programming/other-docs/20A_ITT1_PROGRAMMING_OLA16.html)

## Exercise 4 - Python for everybody chapter exercises

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete chapter 8 exercises, in Python For Everybody.
**remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**

\pagebreak