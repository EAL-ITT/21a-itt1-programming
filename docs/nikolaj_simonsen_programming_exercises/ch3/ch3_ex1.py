'''
Exercise 1: Rewrite your pay computation to give the employee 1.5
times the hourly rate for hours worked above 40 hours.

Enter Hours: 45
Enter Rate: 10
Pay: 475.0
'''

regular_hours = 40
hours = float(input('Enter Hours: '))
rate = float(input('Enter rate: '))
over_rate = rate * 1.5

if hours > 40 :
    over_hours = hours - regular_hours
    print('You worked overtime this week!\n')
    print('Pay: ' + str(((over_hours)*(over_rate))+((regular_hours*rate))))
else :
    print('Pay: ' +str(hours*rate))


