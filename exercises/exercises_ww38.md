---
Week: 38
tags:
- Flowchart
- Conditionals
- Exeption handling
---

# Exercises for ww38
 
The cooperative learning structures can be found at [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf)

## Exercise 1 - Python for everybody chapter 3 knowledge sharing teams

### Information

This exercise recaps what you have been reading in chapter 3 of Python for everybody.

### Exercise instructions

1. In your team use the shared online document and **Round table** to share your understanding of:

    * What is the difference between conditional execution and alternative execution?
    * What is the purpose of chained conditionals and what is the syntax for nested conditionals?
    * What are exeptions in Python and how can you handle them ? Use examples.

Use 10 minutes for all questions.

2. Use **circle of knowledge** for 10 minutes to discuss and agree on your answers.  
Note your agreed answers.

## Exercise 2 - Flowchart basic symbols

### Information

In your team use the shared online document and **Round table** to learn about flowchart symbols.  

### Exercise instructions

1. Read about flowchart basic symbols [https://www.gliffy.com/blog/how-to-flowchart-basic-symbols-part-1-of-3](https://www.gliffy.com/blog/how-to-flowchart-basic-symbols-part-1-of-3) and note your findings.
2. Use **Round table** to discuss your findings

You have 15 minutes.

\pagebreak

## Exercise 3 - Creating flowcharts

### Information

In your team use draw.io [https://app.diagrams.net/](https://app.diagrams.net/), lucidcharts [https://www.lucidchart.com/pages/](https://www.lucidchart.com/pages/), yED [https://www.yworks.com/products/yed](https://www.yworks.com/products/yed) or similar to draw a flowchart of a simple IoT sensor's functionality.

### Exercise instructions

1. Imagine that you are building an IoT sensor system that can measure temperature (Celcius), humidity (%) and barometric pressure (hPa).
The sensor is connected to the internet and responds with a message conaining the corresponding measurement when receiving these commands:

    1. `GET_temperature_C`
    2. `GET_temperature_F`
    3. `GET_pressure`
    4. `GET_humidity`
    5. `GET_dewpoint`  
    **hint** *what is dewpoint?* [https://www.omnicalculator.com/physics/dew-point#whatis](https://www.omnicalculator.com/physics/dew-point#whatis)

2. The objective is to produce a flowchart that gives an overview of your program's flow of operation, using the correct flowchart symbols.

    You do not have to show how to calculate dewpoint in the flowchart, but illustrate that the calculation takes place on the IoT sensor system when the measurement is requested.  

**You have 45 minutes to complete the exercise, I will pick 2 teams to present their flowchart on class.**

\pagebreak

## Exercise 4 - Python for everybody chapter 3 exercises

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

1. Complete chapter 3 exercises, in Python For Everybody.

2. **Remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**

\pagebreak