---
Week: 45
tags:
- Tuple
- Matplotlib
- Visualization
---

# Exercises for ww45

## Exercise 1 - PY4E chapter 10 knowledge (group) 

### Information

In your team discuss your understanding of:

1. How can you create a tuple?
2. Can you change tuples after instantiation?
3. How does the slice operator work on tuples?
4. How do you compare tuples?
5. How does sort work on a tuple?   
6. How do you convert a dictionary to a list of tuples?
7. How do you sort a dictionary into a list ordered by value using tuples

### Exercise instructions

Agree in your team on answers and complete the quiz at: [https://forms.gle/wikgmFVuFNzoDFXV6](https://forms.gle/wikgmFVuFNzoDFXV6)

You have 30 minutes to complete exercise 0

*Remember to note your answers in your team's shared document*

## Exercise 2 - Managing python virtual environments 

### Information

In Python, you can install libraries either system wide, or in a virtual environment like [venv](https://docs.python.org/3/library/venv.html) or [pipenv](https://pypi.org/project/pipenv/).

A virtual environment is like a sandbox for your Python projects, a virtual environment has it's own Python installation and 3rd party library installation is only accessible from within the virtual environment (when the virtual environment is activated).   

*It is recommended to always use a virtual environment when installing 3rd party libraries, this is to avoid library version mismatch between your python projects.*    

### Exercise instructions

*Depending on your choice of IDE use either 1. or 2.*

1.  **Install packages in pycharm**  
By default pycharm uses a virtual environment for each project by default but the setup is a little different compared to pip3 (see 2.).  
To install matplotlib in pycharm follow this article [https://www.jetbrains.com/help/pycharm/installing-uninstalling-and-upgrading-packages.html](https://www.jetbrains.com/help/pycharm/installing-uninstalling-and-upgrading-packages.html)  

2. **Install a virtual environment from git bash (other IDE's)**  
A new venv virtual environment can be set up with the CLI in GIT bash using the below commands:
    1. create the environment:  
    `py -m venv env`
    2. activate the environment:   
    `env\Scripts\activate.bat`
    3. to deactivate the environment:  
    `deactivate`      

## Exercise 3 - Matplotlib installation

### Information

This exercise instructs you on how to install matplotlib.  
The instructions is using pip3 (also known as pip).  
pip3 is the official package manager for Python3, it is used to install 3rd party libraries in python.    

### Exercise instructions

1. activate your virtual environment
2. Install matplotlib in the virtual environment using pip3:  
    `pip3 install matplotlib` 
  
*If you are using pycharm use this article [https://www.jetbrains.com/help/pycharm/installing-uninstalling-and-upgrading-packages.html](https://www.jetbrains.com/help/pycharm/installing-uninstalling-and-upgrading-packages.html)*    


## Exercise 4 - Matplotlib visualization

### Information

This exercise is about using a 3rd party module called matplotlib [https://matplotlib.org/](https://matplotlib.org/). It is a library for visualizing data in various diagrams and charts (line, bar, pie etc.)

Since we have been making histograms in the exercises for chapter 9 we will use those as the data input for the charts.

We will use the matplotlib video series [https://www.youtube.com/playlist?list=PL-osiE80TeTvipOqomVEeZ1HRrcEvtZB_](https://www.youtube.com/playlist?list=PL-osiE80TeTvipOqomVEeZ1HRrcEvtZB_) by Corey Schafer

### Exercise instructions

1. Make a line chart of the data from PY4E ch9_ex2 or ch9_ex5 following video1 [https://youtu.be/UO98lJQ3QGI](https://youtu.be/UO98lJQ3QGI) just skip the install part if you are on pycharm (start at 2:00)  
*Important! matplotlib uses seperate lists for the x and y axis.*  
*ch9_ex2 and ch9_ex5 will give you a dictionary that you have to convert into one list containing the keys and another list containing the values.*  
2. Make a bar chart of the data from PY4E ch9_ex2 or ch9_ex5 following video1 [https://youtu.be/UO98lJQ3QGI](https://youtu.be/UO98lJQ3QGI)  

**Bonus challenge** Make a chart of choice from ch10_Ex3 

## Exercise 5 - Python for everybody chapter exercises

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete chapter 10 exercises, in Python For Everybody.
**remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**

\pagebreak