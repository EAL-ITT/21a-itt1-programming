---
Week: 47
tags:
- Your own challenge
- Catch up
---

# Exercises for ww47

## Exercise 1 - Your own challenge OLA16

You have time this week to work on your own challenge OLA16.

## Exercise 1 - Exercises catch up

### Information

To make sure that you did all the exercises, including exercises from this week you need to make a todo list in your gitlab project `readme.md` file.
This is a nice way to get an overview of the exercises you did not complete yet.  

Completion of the exercises will also help to prepare you for the exam at the end of the semester.

### Exercise instructions

1. In your gitlab projects `readme.md` Make a todo list of all the exercises from the programming course.  
2. For each exercise in the list, check to see that you did all the deliverables (documentation, files etc.) and put a checkmark on that exercise
3. Make a written plan on how you are going to catch up on exercises if you did not do all the exercises yet.

Suggested TODO list layout:  

``` 
- [ ] Week 37
    - [ ] Exercise 1 - xxxxxxx
    - [ ] Exercise 2 - xxxxxxx
    - [ ] Exercise 3 - xxxxxxx
    - [ ] Exercise 4 - xxxxxxx
- [ ] Week xx
    - [ ] Exercise 1 - xxxxxxx
    - [ ] Exercise 1 - xxxxxxx
```

The `- [ ]` syntax will turn into a clickable checkbox in the gitlab `readme.md` file


\pagebreak