---
Week: 43
Content:  Lists
Material: See links in weekly plan
Initials: NISI
---

# Week 43 ITT1-programming - Lists

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Chapter 8 exercises, in Python For Everybody, completed
* Work on your own challenge

### Learning goals

The student can:

* Implement Lists
* Nest lists
* Traverse lists
* Use list operations (+, *, slice)
* Use list functions
* Mutate lists 
* Use lists in functions

The student knows: 

* The list type
* When to use a list
* List methods
* List reference vs. list copy

## Deliverables

* Chapter 8 exercises, in Python For Everybody, documented on Gitlab

## Schedule

* 8:15 Introduction to the day (K1)
* 8:30 Python quiz (Exercise 1 - chapter 6 + 7 knowledge) (K1)
* 9:15 Preparation for hands-on time (K2)  
    Either/or:
    * Read chapter 8
    * Watch chapter 8 videos 
* 10:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 13:45 Exercises without lecturer (K2/K3)
* 15:30 End of day


## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww43](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww43)

## Comments

PY4E Chapter 8 video lessons:

* part 1 [https://youtu.be/ljExWqnWQvo](https://youtu.be/ljExWqnWQvo)
* part 2 [https://youtu.be/bV1FQUBIApM](https://youtu.be/bV1FQUBIApM)
* part 3 [https://youtu.be/GxADdpo6EP4](https://youtu.be/GxADdpo6EP4)